import exp from 'express';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let app = exp();

app.use(exp.json());

app.use((req, res, next) => {
    req.srcFolder = '/ds/';
    req.src = 'rows.json';
    req.filePath = __dirname;
    next();
});

app.get('/rows', readFileDS);

app.post('/row/create', mkDirectory, checkFileExist, (req, res) => {
    res.json(req.body);
});

app.delete('/row/delete', exp.text(), deleteFilePart, (req, res) => {
    res.end();
});

app.post('/row/edit', readEditFile);

app.post('/row/filter', filterFilePart);

app.listen(9000, (err) => {
    if (err)
        console.log('Error:' + err);
    else
        console.log(`It' fine`);
});

app.use((req, res) => {
    if (req.method === 'GET') {
        loadPage(res, req.url, getContentType(req.url));
    }
});


function mkDirectory(req, res, next) {
    fs.mkdir(path.join(req.filePath + req.srcFolder), (err) => {
        if (err) {
            if (err.code === 'EEXIST')
                console.log('Folder already exists');
            else
                next(err);
        }
    });
    next();
}

function checkFileExist(req, res, next) {
    fs.readdir(req.filePath + req.srcFolder, (err, files) => {
        if (err) {
            next(err);
        }
        else {
            if (files.includes(req.src)) {
                readWriteFile(req, next);
            }
            else {
                createWriteFile(req, next);
            }
        }
    });
}

function readFileDS(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, dataStore) => {
        if (err) {
            next(err);
        }
        else {
            res.json(JSON.parse(dataStore).data);
        }
    });
}

function readWriteFile(req, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        if (err) {
            next(err);
        }
        else {
            const dataStore = JSON.parse(data);
            if (dataStore.unused.length) {
                req.body.id = dataStore.unused[dataStore.unused.length - 1];
                dataStore.unused.splice(-1, 1);
            }
            else {
                req.body.id = dataStore.data.length;
            }
            console.log(req.body);
            dataStore.data.push(req.body);
            fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify(dataStore), (err) => {
                if (err) next(err);
            });
        }
        next();
    });
}

function createWriteFile(req, next) {
    const dataStore = {
        data: [],
        unused: [],
    };
    req.body.id = 0;
    dataStore.data.push(req.body);
    fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify(dataStore), (err) => {
        if (err)
            next(err);
        next();
    });
}

function readEditFile(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        if (err) {
            next(err);
        }
        else {
            const dataStore = JSON.parse(data);
            for (let item of dataStore.data) {
                if (item.id === req.body.id) {
                    for (let key in item) {
                        if (!(key === 'row-date')) {
                            item[key] = req.body[key];
                        }
                    }
                }
            }
            fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify(dataStore), (err) => {
                if (err) next(err);
            });
            res.send(JSON.stringify(dataStore.data));
        }
    });
}

function deleteFilePart(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        if (err) {
            next(err);
        }
        else {
            let dataStore = JSON.parse(data);
            if (dataStore.data.length > 1) {
                dataStore.data = dataStore.data.filter(item => (item.id !== +req.body));
                dataStore.unused.push(+req.body);
            }
            fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify(dataStore), (err) => {
                if (err) next(err);
            });
        }
        next();
    });
}

function filterFilePart(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        if (err) {
            next(err);
        }
        else {
            if (req.body['table-search'] === '')
                res.json(JSON.parse(data).data);
            else {
                const dataStore = JSON.parse(data);
                var arrRes = [];
                for (let item of dataStore.data) {
                    if (item[req.body['table-head']] == +item[req.body['table-head']])
                        item.num = true;
                    switch (req.body['table-cond']) {
                        case 'equals':
                            if (item[req.body['table-head']] === req.body['table-search'])
                                arrRes.push(item);
                            break;
                        case 'includes':
                            const str = req.body['table-search'].match(/[?^.,_]?/) ? `\\${req.body['table-search']}` : req.body['table-search'];
                            const regEx = new RegExp(str, 'gi');
                            if (item[req.body['table-head']].match(regEx)) {
                                arrRes.push(item);
                            }
                            break;
                        case 'more':
                            if (item.num) {
                                if (+item[req.body['table-head']] > +req.body['table-search'])
                                    arrRes.push(item);
                            }
                            else {
                                if (item[req.body['table-head']] > req.body['table-search'])
                                    arrRes.push(item);
                            }
                            break;
                        case 'less':
                            if (item.num) {
                                if (+item[req.body['table-head']] < +req.body['table-search'])
                                    arrRes.push(item);
                            }
                            else {
                                if (item[req.body['table-head']] < req.body['table-search'])
                                    arrRes.push(item);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        res.json(arrRes);
    });
}

app.use(errHandler);

function errHandler(err, req, res) {
    if (err)
        res.json({ 'error': err });
}

function loadPage(res, name, conType) {
    const file = path.join(__dirname + name);
    fs.readFile(file, (err, data) => {
        if (err) {
            res.writeHead(404);
            if (name.match('rows')) {
                res.write('No rows yet');
            }
        }
        else {
            res.writeHead(200, { 'Content-Type': conType });
            res.write(data);
        }
        res.end();
    })
}

function getContentType(url) {
    let ext = path.extname(url);
    switch (ext) {
        case '.html':
            return 'text/html';
        case '.css':
            return 'text/css';
        case '.js':
            return 'text/javascript';
        case '.json':
            return 'application/json';
        default:
            return 'multipart/form-data';
    }
}