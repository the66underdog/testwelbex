import React from 'react';

function TipEdit({ isDisp }) {

    return (
        <fieldset className='tip-msg' style={{ backgroundColor: 'rgb(75, 121, 210)', opacity: isDisp ? '1' : '0', transform: isDisp ? 'translateY(0%)' : 'translateY(100%)' }}>
            <legend><span className='text-imp'>TIP</span> of the day:</legend>
            To <span className='text-imp'>EDIT</span> a row
            press <span className='text-imp'>LEFT MOUSE BUTTON</span> with cursor on it
            and <span className='text-imp'>FILL</span> inputs you'd like to change
        </fieldset>
    );
}

export default TipEdit;