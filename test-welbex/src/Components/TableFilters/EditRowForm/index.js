import React, { useContext, useRef } from 'react';
import { MainContext } from '../../../App';

function EditRowForm({ isDisp, idx }) {

    const refForm = useRef();
    const contextObj = useContext(MainContext);
    let defyIndex = idx || idx === 0 ? true : false;

    function handleInputChange(e) {
        if (e.target.name === 'row-name' && e.target.value.length === 0) {
            e.target.value = contextObj.rows[idx]['row-name'];
        }
        if (e.target.name === 'row-quant' && e.target.value.length === 0) {
            e.target.value = contextObj.rows[idx]['row-quant'];
        }
        if (e.target.name === 'row-dist' && e.target.value.length === 0) {
            e.target.value = contextObj.rows[idx]['row-dist'];
        }
    }

    return (
        <form ref={refForm} className={`form-row form-row__edit ${isDisp ? 'form-row_enabled' : ''}`} method='POST'
            onSubmit={e => { e.preventDefault(); contextObj.edit(refForm.current, idx); e.target.reset(); }}>
            <fieldset className='fieldset-name-quant-dist'>
                <legend>Editing row</legend>
                <div className='row-name'>
                    <label htmlFor='row-name'><span className='text-imp'>Name</span> : {defyIndex ? `${contextObj.rows[idx]['row-name']} -->` : null}</label>
                    <input type='text' id='row-name' name='row-name' disabled={!defyIndex} onBlur={handleInputChange} defaultValue={defyIndex ? `${contextObj.rows[idx]['row-name']}` : null} />
                </div>
                <div className='row-quant'>
                    <label htmlFor='row-quant'><span className='text-imp'>Quantity</span> : {defyIndex ? `${contextObj.rows[idx]['row-quant']} -->` : null}</label>
                    <input type='text' id='row-quant' name='row-quant' disabled={!defyIndex} onBlur={handleInputChange} defaultValue={defyIndex ? `${contextObj.rows[idx]['row-quant']}` : null} />
                </div>
                <div className='row-dist'>
                    <label htmlFor='row-dist'><span className='text-imp'>Distance</span> : {defyIndex ? `${contextObj.rows[idx]['row-dist']} -->` : null}</label>
                    <input type='text' id='row-dist' name='row-dist' disabled={!defyIndex} onBlur={handleInputChange} defaultValue={defyIndex ? `${contextObj.rows[idx]['row-dist']}` : null} />
                </div>
            </fieldset>
            <button type='submit' disabled={!defyIndex}>Submit changes</button>
        </form>
    );
}

export default EditRowForm;