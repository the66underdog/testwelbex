import React from 'react';
import './style.css';
import TableRow from '../TableRow/';

function Table({ rows, deleteMode, editMode, deleteRow, setRowId }) {
    
    function handleMouseEvent(e) {
        const row = e.target.closest('tr');
        if (deleteMode) {
            switch (e.type) {
                case 'mouseover':
                    row.style.cssText = `
                    background-color: red;
                    color: white;
                    cursor: pointer;
                `;
                    break;
                case 'mouseout':
                    row.removeAttribute('style');
                    break;
                case 'click':
                    deleteRow(+row.lastElementChild.innerHTML);
                    break;
                default:
                    break;
            }
        }
        if (editMode) {
            switch (e.type) {
                case 'mouseover':
                    row.style.cssText = `
                    background-color: rgb(75, 121, 210);
                    color: white;
                    cursor: pointer;
                `;
                    break;
                case 'mouseout':
                    row.removeAttribute('style');
                    break;
                case 'click':
                    setRowId(+row.lastElementChild.innerHTML);
                    break;
                default:
                    break;
            }
        }
    }

    return (
        <>
            <table className='table-main'>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Distance</th>
                    </tr>
                </thead>
                <tbody onMouseOver={handleMouseEvent} onMouseOut={handleMouseEvent} onClick={handleMouseEvent}>
                    {rows.map((item, index) => (<TableRow key={item.id} row={item} idx={index} />))}
                </tbody>
            </table>
        </>
    );
}

export default Table;