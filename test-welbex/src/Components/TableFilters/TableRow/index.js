import React from 'react';
import './style.css';

function TableRow({ row, idx }) {

    return (
        <tr className={idx % 2 === 0 ? 'even' : 'odd'}>
            <td>{row['row-date']}</td>
            <td>{row['row-name']}</td>
            <td>{row['row-quant']}</td>
            <td>{row['row-dist']}</td>
            <td style={{display: 'none'}}>{row.id}</td>
        </tr>
    );
}

export default TableRow;