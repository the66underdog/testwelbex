import React, { useRef } from 'react';
import './style.css';

function Filters({ isDisp, applyFilters, setRowId, setCurrentPage }) {

    const refForm = useRef();

    return (
        <form ref={refForm} className='table-filters' onSubmit={(e) => { e.preventDefault(); setCurrentPage(1);  setRowId(null); applyFilters(refForm.current); }}
            style={{ opacity: isDisp ? '1' : '0', transform: isDisp ? 'translateY(0%)' : 'translateY(-100%)' }}>
            <fieldset className='column-filters'>
                <legend>Column filters</legend>
                <div>
                    <label htmlFor='table-head'><span className='text-imp'>Column</span> :</label>
                    <select id='table-head' name='table-head'>
                        <option value='row-name'>Name</option>
                        <option value='row-quant'>Quantity</option>
                        <option value='row-dist'>Distance</option>
                    </select>
                </div>
                <div>
                    <label htmlFor='table-cond'><span className='text-imp'>Condition</span> :</label>
                    <select id='table-cond' name='table-cond'>
                        <option value='equals'>Equal</option>
                        <option value='includes'>Includes</option>
                        <option value='more'>More</option>
                        <option value='less'>Less</option>
                    </select>
                </div>
            </fieldset>
            <fieldset className='keyword-search'>
                <legend>Search key-value</legend>
                <label htmlFor='table-search'><span className='text-imp'>Key-value</span> :</label>
                <input type='text' name='table-search' />
            </fieldset>
            <button type='submit'>Filter</button>
        </form>
    );
}

export default Filters;