import React from 'react';
import './style.css';

function Pagination({ rows, curPage, setCurPage, rowAmountPerPage }) {

    const pages = Array.from({ length: Math.ceil(rows.length / rowAmountPerPage) }, (_, index) => index + 1);

    function goBack() {
        if (curPage !== 1) {
            setCurPage(prev => prev - 1);
        }
    }

    function goForward() {
        if (curPage !== Math.ceil(rows.length / rowAmountPerPage)) {
            setCurPage(prev => prev + 1);
        }
    }

    return (
        <>
            <ul className='pagination'>
                <li><button className='button-side' onClick={goBack}>Prev</button></li>
                {pages.map((item, index) => (<li key={index}>
                    <button className={`button-number ${item === curPage ? 'active' : ''}`}
                        onClick={() => { setCurPage(item) }}>
                        <span className='text-imp'>{item}</span>
                    </button>
                </li>))}
                <li><button className='button-side' onClick={goForward}>Next</button></li>
            </ul>
        </>
    );
}

export default Pagination;