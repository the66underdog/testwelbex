import React from 'react';
import './style.css';

function TipDelete({ isDisp }) {

    return (
        <fieldset className='tip-msg' style={{ backgroundColor: 'darkred', opacity: isDisp ? '1' : '0', transform: isDisp ? 'translateY(0%)' : 'translateY(100%)' }}>
            <legend><span className='text-imp'>TIP</span> of the day:</legend>
            To <span className='text-imp'>DELETE</span> a row
            press <span className='text-imp'>LEFT MOUSE BUTTON</span> with cursor on it
        </fieldset>
    );
}

export default TipDelete;