import React, { useEffect, useState } from 'react';
import AddRowForm from '../AddRowForm';
import TipDelete from '../TipDelete';
import TipEdit from '../TipEdit';
import EditRowForm from '../EditRowForm';
import './style.css';

function SideMenu({ setDispFilters, deleteMode, setDeleteMode, editMode, setEditMode, rowId, setRowId }) {

    const [addForm, setAddForm] = useState(false);
    const [editForm, setEditForm] = useState(false);

    function handleSideMenu(e) {
        if (e.target.closest('.side-menu__add')) {
            setAddForm(prev => !prev);
        }
        if (e.target.closest('.side-menu__delete')) {
            setDeleteMode(prev => !prev);
        }
        if (e.target.closest('.side-menu__edit')) {
            setEditMode(prev => !prev);
        }
        if (e.target.closest('.side-menu__filters')) {
            setDispFilters(prev => !prev);
        }
    }

    useEffect(() => {
        if (addForm) {
            setDeleteMode(false);
            setEditMode(false)
        }
    }, [addForm, setDeleteMode, setEditMode]);

    useEffect(() => {
        if (deleteMode) {
            setAddForm(false);
            setEditMode(false);
        }
    }, [deleteMode, setEditMode]);

    useEffect(() => {
        setEditForm(editMode);
        setDeleteMode(false);
        if (!editMode)
            setRowId(null);
    }, [editMode, setDeleteMode, setRowId]);

    return (
        <>
            <div className='side-menu' onClick={handleSideMenu}>
                <div className='side-menu__item side-menu__add'>
                    <img src={`${process.env.PUBLIC_URL}/Images/Cross.png`} alt='Add' />
                    <span>Create</span>
                </div>
                <div className='side-menu__item side-menu__delete'
                    style={{ boxShadow: deleteMode ? 'inset -4px -4px 12px red' : 'none' }}>
                    <img src={`${process.env.PUBLIC_URL}/Images/Cross.png`}
                        style={{ backgroundColor: deleteMode ? 'darkred' : 'rgb(37, 49, 42)' }} alt='Add' />
                    <span>Delete mode: {deleteMode ? <span style={{ color: 'red' }}>ON</span> : <span>OFF</span>}</span>
                </div>
                <div className='side-menu__item side-menu__edit'
                    style={{ boxShadow: editMode ? 'inset -4px -4px 8px rgba(144, 177, 240, 0.75)' : 'none' }}>
                    <img src={`${process.env.PUBLIC_URL}/Images/Edit.png`}
                        style={{ backgroundColor: editMode ? 'rgb(75, 121, 210)' : 'rgb(37, 49, 42)' }} alt='Edit' />
                    <span>Edit</span>
                </div>
                <div className='side-menu__item side-menu__filters'>
                    <img src={`${process.env.PUBLIC_URL}/Images/Filter.png`} alt='Filters' />
                    <span>Filters</span>
                </div>
            </div>
            <AddRowForm isDisp={addForm} />
            <EditRowForm isDisp={editForm} idx={rowId} />
            <TipDelete isDisp={deleteMode} />
            <TipEdit isDisp={editMode} />
        </>
    );
}

export default SideMenu;
