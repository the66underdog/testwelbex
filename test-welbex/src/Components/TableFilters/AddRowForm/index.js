import React, { useContext, useRef } from 'react';
import { MainContext } from '../../../App';

import './style.css';

function AddRowForm({ isDisp }) {

    const refForm = useRef();
    const contextObj = useContext(MainContext);

    return (
        <form ref={refForm} className={`form-row form-row__add ${isDisp ? 'form-row_enabled' : ''}`} method='POST'
            onSubmit={e => { e.preventDefault(); contextObj.post(refForm.current); e.target.reset(); }}>
            <fieldset className='fieldset-name-quant-dist'>
                <legend>Adding row</legend>
                <div className='row-name'>
                    <label htmlFor='row-name'>Name :</label>
                    <input type='text' id='row-name' name='row-name' required />
                </div>
                <div className='row-quant'>
                    <label htmlFor='row-quant'>Quantity :</label>
                    <input type='text' id='row-quant' name='row-quant' required />
                </div>
                <div className='row-dist'>
                    <label htmlFor='row-dist'>Distance :</label>
                    <input type='text' id='row-dist' name='row-dist' required />
                </div>
            </fieldset>
            <button type='submit'>+ Add</button>
        </form>
    );
}

export default AddRowForm;