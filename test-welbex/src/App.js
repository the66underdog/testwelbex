import React, { createContext, useEffect, useState } from 'react';
import './App.css';
import Table from './Components/TableFilters/Table';
import Filters from './Components/TableFilters/Filters';
import SideMenu from './Components/TableFilters/SideMenu';
import Pagination from './Components/TableFilters/Pagination';

export const MainContext = createContext();

function App() {
  const [rows, setRows] = useState([]);
  const [filters, setFilters] = useState(false);
  const [deleteMode, setDeleteMode] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [rowId, setRowId] = useState();

  const [currentPage, setCurrentPage] = useState(1);
  const rowAmountPerPage = 10;


  function postRow(form) {
    const date = new Date();
    let hours = date.getHours();
    let apM = hours < 12 ? 'AM' : 'PM';
    let minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    const dateStr = `${date.getDate()}.${date.getMonth()}.${date.getFullYear()} ${hours}:${minutes} ${apM}`;
    const req = new XMLHttpRequest();
    const formData = new FormData(form);
    const obj = {
      'row-date': dateStr,
      'row-name': formData.get('row-name'),
      'row-quant': formData.get('row-quant'),
      'row-dist': formData.get('row-dist'),
    };
    req.open('POST', `/row/create`, true);
    req.onload = () => {
      if (req.status === 200)
        setRows(prev => [...prev, JSON.parse(req.response)]);
    }
    req.onerror = err => { throw err = new Error() };
    req.setRequestHeader('Content-Type', 'application/json');
    req.send(JSON.stringify(obj));
  }

  function deleteRow(idx) {
    const req = new XMLHttpRequest();
    req.open('DELETE', '/row/delete', true);
    req.onload = () => {
      if (req.status === 200) {
        setRows(prev => prev.filter(item => item.id !== idx));
      }
    }
    req.setRequestHeader('Content-Type', 'text/plain');
    req.send(idx);
  }

  function editRow(form, idx) {
    const req = new XMLHttpRequest();
    const formData = new FormData(form);
    req.open('POST', '/row/edit', true);
    const obj = {
      'row-name': formData.get('row-name'),
      'row-quant': formData.get('row-quant'),
      'row-dist': formData.get('row-dist'),
    };
    req.onload = () => {
      if (req.status === 200) {
        setRows(JSON.parse(req.response));
      }
    }
    req.onerror = err => {
      throw err = new Error();
    }
    req.setRequestHeader('Content-Type', 'application/json');
    obj.id = idx;
    req.send(JSON.stringify(obj));
  }

  function applyFilters(form) {
    const req = new XMLHttpRequest();
    const formData = new FormData(form);
    req.open('POST', '/row/filter', true);
    const obj = {
      'table-head': formData.get('table-head'),
      'table-cond': formData.get('table-cond'),
      'table-search': formData.get('table-search'),
    };
    req.onload = () => {
      if (req.status === 200) {
        setRows(JSON.parse(req.response));
      }
    }
    req.onerror = err => {
      throw err = new Error();
    }
    req.setRequestHeader('Content-Type', 'application/json');
    req.send(JSON.stringify(obj));
  }

  function getDataStore(url, cbState, conType) {
    const req = new XMLHttpRequest();
    req.open('GET', url, true);
    req.onload = () => {
      if (req.status === 200) {
        cbState(JSON.parse(req.response));
      }
      if (req.status === 404) {
        console.log(req.response);
      }
    }
    req.onerror = err => { throw err = new Error() };
    req.setRequestHeader('Content-Type', conType);
    req.send();
  }

  useEffect(() => {
    getDataStore('/rows', setRows, 'application/json');
  }, []);

  const lastRowIndex = currentPage * rowAmountPerPage;
  const firstRowIndex = lastRowIndex - rowAmountPerPage;

  const curPageRows = rows.slice(firstRowIndex, lastRowIndex)

  return (
    <div className='app-container'>
      <Filters isDisp={filters} applyFilters={applyFilters} setRowId={setRowId} setCurrentPage={setCurrentPage} />
      <Pagination rows={rows} curPage={currentPage} setCurPage={setCurrentPage} rowAmountPerPage={rowAmountPerPage} />
      <Table rows={curPageRows} deleteMode={deleteMode} editMode={editMode} deleteRow={deleteRow} setRowId={setRowId} />
      <MainContext.Provider value={{ rows: rows, post: postRow, delete: deleteRow, edit: editRow }}>
        <SideMenu setDispFilters={setFilters} deleteMode={deleteMode} setDeleteMode={setDeleteMode}
          editMode={editMode} setEditMode={setEditMode} rowId={rowId} setRowId={setRowId} />
      </MainContext.Provider>
    </div >
  );
}

export default App;